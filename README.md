

### Install & Run ###

#####This project is a simple Maven project ( you can import/run it into STS or Eclipse).#####

##### How to run #####
* To run the application, Please import the project into eclipse and run the application with the class App.java

* How to stop : Followed by “>>>” prompt input “quit”, system will exit.

* How to play : Followed by “>>>”  you can input the command as followed. 
place 1, 2, north
move
left
right
report
quit



### Description ###

* This sample project follows the traditional java thinking with Command Design Patten. The main benefit of this approve is that the actions/commands of the robot could be easily extended. The one of drawback compared to the functional programming is that there are lots of interfaces and classes.

* In addition, all classes are trying to make them immutable.



