package au.com.test.javarobot.core.impl.cmd;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import au.com.test.javarobot.IRobot;
import au.com.test.javarobot.IRobotCmd;
import au.com.test.javarobot.Position;
import au.com.test.javarobot.core.impl.FaceDirection;
import au.com.test.javarobot.core.impl.TableTop;
import au.com.test.javarobot.core.impl.Robot;
import au.com.test.javarobot.core.impl.cmd.LeftCmd;

public class LeftCmdTest {
	private IRobotCmd<Position> left;

	@Before
	public void setUp() throws Exception {
		left = new LeftCmd();
	}

	@Test
	public void testExecuteBy() throws Exception {
		IRobot robot = new Robot(new TableTop(5, 5) );
		Position position = new Position(1, 1, FaceDirection.WEST);
		robot.moveTo(position);
		left.executeBy(robot);
		assertEquals(robot.getCurrentPosition(),
				new Position(1, 1, FaceDirection.SOUTH));
	}

}
