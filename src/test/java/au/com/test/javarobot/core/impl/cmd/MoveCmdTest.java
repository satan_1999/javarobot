package au.com.test.javarobot.core.impl.cmd;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import au.com.test.javarobot.IRobot;
import au.com.test.javarobot.IRobotCmd;
import au.com.test.javarobot.Position;
import au.com.test.javarobot.core.impl.FaceDirection;
import au.com.test.javarobot.core.impl.TableTop;
import au.com.test.javarobot.core.impl.Robot;
import au.com.test.javarobot.core.impl.cmd.MoveCmd;

public class MoveCmdTest {
	private IRobotCmd<Position> move;

	@Before
	public void setUp() throws Exception {
		move = new MoveCmd();
	}

	@Test
	public void testExecuteBy() throws Exception {
		IRobot robot = new Robot(new TableTop(5, 5) );
		Position position = new Position(1, 1, FaceDirection.WEST);
		Position targetPosition = new Position(0, 1, FaceDirection.WEST);
		robot.moveTo(position);
		move.executeBy(robot);
		assertEquals(robot.getCurrentPosition(),
				targetPosition);
	}

}
