package au.com.test.javarobot.core.impl;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import au.com.test.javarobot.IRobot;
import au.com.test.javarobot.Position;
import au.com.test.javarobot.core.impl.TableTop;
import au.com.test.javarobot.core.impl.Robot;
import au.com.test.javarobot.exception.FallOffTableException;
import au.com.test.javarobot.exception.NotPlacedException;

public class RobotTest {
	private IRobot robot;

	@Before
	public void setUp() throws Exception {
		robot = new Robot(new TableTop(5, 5));
	}

	@Test
	public void testMoveTo() throws FallOffTableException, NotPlacedException {
		Position position = new Position(1, 2, FaceDirection.WEST);
		robot.moveTo(position);
		assertEquals(position, robot.getCurrentPosition());
	}
	
	@Test(expected=FallOffTableException.class)
	public void testMoveToWithException() throws FallOffTableException {
		Position position = new Position(-1, 2, FaceDirection.WEST);
		robot.moveTo(position);
		
	}
	
	@Test(expected=NotPlacedException.class)
	public void testGetCurrentPositionWithException() throws NotPlacedException {
		robot.getCurrentPosition();
	}

}
