package au.com.test.javarobot.core.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import au.com.test.javarobot.Position;

public class TableTopTest {
	private TableTop tableTop;
	
	@Before
	public void setup() {
		tableTop = new TableTop(5, 5);
	}

	@Test
	public void testIsFallOff() {
		Position out1 = new Position(-1, 3, FaceDirection.EAST);
		assertTrue(tableTop.isFallOff(out1));
		
		out1 = new Position(5, 3, FaceDirection.EAST);
		assertTrue(tableTop.isFallOff(out1));
		
		out1 = new Position(1, -1, FaceDirection.EAST);
		assertTrue(tableTop.isFallOff(out1));
		
		out1 = new Position(1, 5, FaceDirection.EAST);
		assertTrue(tableTop.isFallOff(out1));
		
		Position in = new Position(1, 1, FaceDirection.EAST);
		assertTrue(!tableTop.isFallOff(in));
	}

	@Test
	public void testGetScope() {
		assertEquals("5 * 5", tableTop.getScope());
	}

}
