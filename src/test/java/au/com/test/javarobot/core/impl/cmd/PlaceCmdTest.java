package au.com.test.javarobot.core.impl.cmd;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import au.com.test.javarobot.IRobot;
import au.com.test.javarobot.IRobotCmd;
import au.com.test.javarobot.Position;
import au.com.test.javarobot.core.impl.FaceDirection;
import au.com.test.javarobot.core.impl.TableTop;
import au.com.test.javarobot.core.impl.Robot;
import au.com.test.javarobot.core.impl.cmd.PlaceCmd;

public class PlaceCmdTest {
	private IRobotCmd<Position> place;

	@Before
	public void setUp() throws Exception {
		place = new PlaceCmd(1,2,FaceDirection.EAST);
	}

	@Test
	public void testExecuteBy() throws Exception {
		IRobot robot = new Robot(new TableTop(5, 5) );
		Position position = new Position(1,2,FaceDirection.EAST);
		place.executeBy(robot);
		assertEquals(robot.getCurrentPosition(),
				position);
	}

}
