package au.com.test.javarobot.core.impl.cmd;

import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import au.com.test.javarobot.IRobot;
import au.com.test.javarobot.IRobotCmd;
import au.com.test.javarobot.Position;
import au.com.test.javarobot.core.impl.FaceDirection;
import au.com.test.javarobot.core.impl.TableTop;
import au.com.test.javarobot.core.impl.Robot;
import au.com.test.javarobot.core.impl.cmd.ReportCmd;

public class ReportCmdTest {
	private IRobotCmd<String> report;

	@Before
	public void setUp() throws Exception {
		report = new ReportCmd();
	}

	@Test
	public void testExecuteBy() throws Exception {
		IRobot robot = new Robot(new TableTop(5, 5) );
		Position position = new Position(1, 1, FaceDirection.WEST);
		robot.moveTo(position);
		String result = report.executeBy(robot);
		assertTrue(result.equalsIgnoreCase("Postition [x=1, y=1, face=WEST]"));
	}

}
