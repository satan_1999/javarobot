package au.com.test.javarobot.core;

import static org.junit.Assert.*;

import org.junit.Test;

import au.com.test.javarobot.Position;
import au.com.test.javarobot.core.impl.FaceDirection;

/**
 * Test case for Enum FaceDirection.
 *
 */
public class FaceDirectionTest {

	@Test
	public void testTureLeft() {
		assertTrue(FaceDirection.EAST.turnLeft() == FaceDirection.NORTH);
		assertTrue(FaceDirection.NORTH.turnLeft() == FaceDirection.WEST);
		assertTrue(FaceDirection.WEST.turnLeft() == FaceDirection.SOUTH);
		assertTrue(FaceDirection.SOUTH.turnLeft() == FaceDirection.EAST);
	}
	
	@Test
	public void testTureRight() {
		assertTrue(FaceDirection.EAST.turnRight() == FaceDirection.SOUTH);
		assertTrue(FaceDirection.SOUTH.turnRight() == FaceDirection.WEST);
		assertTrue(FaceDirection.WEST.turnRight() == FaceDirection.NORTH);
		assertTrue(FaceDirection.NORTH.turnRight() == FaceDirection.EAST);
	}
	
	@Test
	public void testMove() {
		Position a = FaceDirection.EAST.moveForwards(2, 2);
		assertTrue(a.equals(new Position(3, 2, FaceDirection.EAST)));
		a = FaceDirection.SOUTH.moveForwards(2, 2);
		assertTrue(a.equals(new Position(2, 1, FaceDirection.SOUTH)));
		a = FaceDirection.WEST.moveForwards(2, 2);
		assertTrue(a.equals(new Position(1, 2, FaceDirection.WEST)));
		a = FaceDirection.NORTH.moveForwards(2, 2);
		assertTrue(a.equals(new Position(2, 3, FaceDirection.NORTH)));
	}

}
