package au.com.test.javarobot.interaction.console;

import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import au.com.test.javarobot.IRobotCmd;
import au.com.test.javarobot.core.impl.cmd.LeftCmd;
import au.com.test.javarobot.core.impl.cmd.MoveCmd;
import au.com.test.javarobot.core.impl.cmd.PlaceCmd;
import au.com.test.javarobot.core.impl.cmd.ReportCmd;
import au.com.test.javarobot.core.impl.cmd.RightCmd;
import au.com.test.javarobot.core.interaction.ICmdHandler;
import au.com.test.javarobot.exception.InvalidCommandException;
import au.com.test.javarobot.interaction.console.CmdHandler;
import au.com.test.javarobot.interaction.console.handlers.LeftCmdHandler;
import au.com.test.javarobot.interaction.console.handlers.MoveCmdHandler;
import au.com.test.javarobot.interaction.console.handlers.PlaceCmdHandler;
import au.com.test.javarobot.interaction.console.handlers.ReportCmdHandler;
import au.com.test.javarobot.interaction.console.handlers.RightCmdHandler;

public class CmdHandlerTest {
	
	private ICmdHandler<String> handler; 

	@Before
	public void setUp() throws Exception {
		handler = new CmdHandler<String>();
		handler.addHandler(new PlaceCmdHandler());
		handler.addHandler(new LeftCmdHandler());
		handler.addHandler(new RightCmdHandler());
		handler.addHandler(new MoveCmdHandler());
		handler.addHandler(new ReportCmdHandler());
	}

	@Test
	public void testRegisterParser() {
		assertTrue(handler.getCmdhandlersCount() == 5);
	}

	@Test
	public void testFindCmdLeft() throws InvalidCommandException {
		String cmdString = " leFt ";
		IRobotCmd<?> cmd = handler.findCmd(cmdString);
		assertTrue(cmd instanceof LeftCmd);
	}
	
	@Test
	public void testFindCmdRight() throws InvalidCommandException {
		String cmdString = " RigHT ";
		IRobotCmd<?> cmd = handler.findCmd(cmdString);
		assertTrue(cmd instanceof RightCmd);
	}
	
	@Test
	public void testFindCmdMove() throws InvalidCommandException {
		String cmdString = " mOVe    ";
		IRobotCmd<?> cmd = handler.findCmd(cmdString);
		assertTrue(cmd instanceof MoveCmd);
	}
	
	@Test
	public void testFindCmdReport() throws InvalidCommandException {
		String cmdString = " rePOrT    ";
		IRobotCmd<?> cmd = handler.findCmd(cmdString);
		assertTrue(cmd instanceof ReportCmd);
	}
	
	@Test
	public void testFindCmdPlace() throws InvalidCommandException {
		String cmdString = "   PLacE 2,  5, nOrth    ";
		IRobotCmd<?> cmd = handler.findCmd(cmdString);
		assertTrue(cmd instanceof PlaceCmd);
	}
	
	@Test(expected=InvalidCommandException.class)
	public void testFindCmdWithUnrecongnizedException() throws 
					InvalidCommandException { 
		String cmdString = "   PLacEEE 2,  5, nOrth    ";
		handler.findCmd(cmdString);
	}
	
	@Test(expected=InvalidCommandException.class)
	public void testWithUnrecongnizedExceptionX() throws 
					InvalidCommandException {
		String cmdString = "   PLacE tsssdf,  5, nOrth    ";
		handler.findCmd(cmdString);
	}
	
	@Test(expected=InvalidCommandException.class)
	public void testPlaceCmdWithUnrecongnizedExceptionY() throws 
					InvalidCommandException {
		String cmdString = "   PLacE 3,  5 w, nOrth    ";
		handler.findCmd(cmdString);
	}
	
	@Test(expected=InvalidCommandException.class)
	public void testPlaceCmdWithUnrecongnizedExceptionFace() throws 
					InvalidCommandException {
		String cmdString = "   PLacE 3,  5, TTTT north";
		handler.findCmd(cmdString);
	}

}
