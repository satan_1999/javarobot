package au.com.test.javarobot;

import au.com.test.javarobot.exception.FallOffTableException;
import au.com.test.javarobot.exception.NotPlacedException;

/**
 * This interface describe the behaviors which the robot should have.
 *
 */
public interface IRobot {
	/**
	 * Move to certain position.
	 * 
	 * @param position 
	 * 		({@link Position}). The position which the robot will move to.
	 * 	
	 * @throws 
	 * 		{@link FallOffTableException}. If the robot move out of table top range.
	 */
	public void moveTo(Position position) throws FallOffTableException;
	
	
	/**
	 * Get robot's current position.
	 * 
	 * @return 
	 * 		({@link Position}). The current position of the robot.
	 * @throws 
	 * 		{@link NotPlacedException}. If the robot hasn't be placed yet.
	 */
	public Position getCurrentPosition()throws NotPlacedException;
}
