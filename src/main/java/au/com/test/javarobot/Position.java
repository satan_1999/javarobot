package au.com.test.javarobot;

import au.com.test.javarobot.core.impl.FaceDirection;

/**
 * Threadsafe.
 * 
 * <p>
 * This is an immutable class to describe the position on the table top.
 * </p>
 * 
 */
public final class Position {
	private final int x;
	private final int y;
	private final FaceDirection face;
	
	public Position(int x, int y, FaceDirection face) {
		super();
		this.x = x;
		this.y = y;
		this.face = face;
	}

	public int getX() {
		return x;
	}
	
	public int getY() {
		return y;
	}
	
	public FaceDirection getFace() {
		return face;
	}
	
	
	public Position move() {
		return this.face.moveForwards(this.x, this.y);
	}
	
	
	public Position clone() {
		return new Position(this.x, this.y, this.face);
	}

	@Override
	public String toString() {
		return "Postition [x=" + x + ", y=" + y + ", face=" + face + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((face == null) ? 0 : face.hashCode());
		result = prime * result + x;
		result = prime * result + y;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Position other = (Position) obj;
		if (face != other.face)
			return false;
		if (x != other.x)
			return false;
		if (y != other.y)
			return false;
		return true;
	}
	
	
}
