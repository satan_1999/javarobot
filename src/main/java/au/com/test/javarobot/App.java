package au.com.test.javarobot;

import au.com.test.javarobot.core.impl.Robot;
import au.com.test.javarobot.core.impl.TableTop;
import au.com.test.javarobot.core.interaction.ICmdHandler;
import au.com.test.javarobot.interaction.console.CmdHandler;
import au.com.test.javarobot.interaction.console.RobotInteractor;
import au.com.test.javarobot.interaction.console.handlers.LeftCmdHandler;
import au.com.test.javarobot.interaction.console.handlers.MoveCmdHandler;
import au.com.test.javarobot.interaction.console.handlers.PlaceCmdHandler;
import au.com.test.javarobot.interaction.console.handlers.ReportCmdHandler;
import au.com.test.javarobot.interaction.console.handlers.RightCmdHandler;

/**
 * Main entry of application.
 * 
 */
public class App {
	public static void main(String[] args) {
		// Define the table
		IRobot robot = new Robot(new TableTop(5, 5));

		ICmdHandler<String> handler = new CmdHandler<String>();

		// Build the responsibility chain of the command parser locator.
		// We could create a new command parser & a Command and register the
		// parser in locator, then the system will have new capability to handle
		// new command.
		handler.addHandler(new PlaceCmdHandler())
				.addHandler(new LeftCmdHandler())
				.addHandler(new RightCmdHandler())
				.addHandler(new MoveCmdHandler())
				.addHandler(new ReportCmdHandler());

		RobotInteractor console = new RobotInteractor(handler, robot);

		console.start();
	}
}
