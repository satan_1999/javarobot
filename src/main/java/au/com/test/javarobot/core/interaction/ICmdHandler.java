package au.com.test.javarobot.core.interaction;

import au.com.test.javarobot.IRobotCmd;
import au.com.test.javarobot.exception.InvalidCommandException;

/**
 * This interface describe the behaviors of command locator.
 * 
 */
public interface ICmdHandler<T> {
	/**
	 * Registe the parse.
	 * 
	 * @param parser
	 * 		({@link IRobotCmdHandler})
	 */
	public ICmdHandler<T> addHandler(IRobotCmdHandler<?, T> parser);
	
	/**
	 * Get registered parsers' total count.
	 * 
	 * @return
	 */
	public int getCmdhandlersCount();
	
	/**
	 * Find the match command parser.
	 * 
	 * @param t
	 * @return
	 * @throws InvalidCommandException
	 */
	public IRobotCmd<?> findCmd(T t) throws InvalidCommandException;
}
