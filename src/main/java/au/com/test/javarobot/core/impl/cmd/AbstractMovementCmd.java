package au.com.test.javarobot.core.impl.cmd;

import au.com.test.javarobot.IMovementCmd;
import au.com.test.javarobot.IRobot;
import au.com.test.javarobot.Position;
import au.com.test.javarobot.exception.FallOffTableException;
import au.com.test.javarobot.exception.NotPlacedException;

public abstract class AbstractMovementCmd implements IMovementCmd {

	public Position executeBy(IRobot robot) throws NotPlacedException, FallOffTableException {
		Position currentPosition = robot.getCurrentPosition();
		Position newPosition     = handlePosition(currentPosition);
		robot.moveTo(newPosition);
		return newPosition;
	}
	
	/**
	 * Leave the logic process by child class.
	 * 
	 * @param currentPosition
	 * @return
	 */
	public abstract Position handlePosition(Position currentPosition);

}
