package au.com.test.javarobot.core.interaction;

import au.com.test.javarobot.IRobotCmd;

/**
 * This interface describe the behaviors of command parser.
 * @param <S>
 * 			Command parser type.
 * @param <T>
 * 			Input type.
 */
public interface IRobotCmdHandler<S, T> {
	/**
	 * Judge current input is concerned.
	 * 
	 * @param t
	 * @return
	 */
	public boolean isConcerned(T t);
	
	/**
	 * Parse the input to real command.
	 * 
	 * @param t
	 * @return
	 * 		The real command supported by app.
	 */
	public IRobotCmd<S> parse(T t);
}
