package au.com.test.javarobot.core.impl.cmd;

import au.com.test.javarobot.Position;

/**
 * Movement command "Right"
 */
public class RightCmd extends AbstractMovementCmd {
	
	/**
	 * @see {@link AbstractMovementCmd#handlePosition(Position)}
	 */
	@Override
	public Position handlePosition(Position currentPosition) {
		return new Position(currentPosition.getX(),
				currentPosition.getY(), currentPosition.getFace().turnRight());
	}

}
