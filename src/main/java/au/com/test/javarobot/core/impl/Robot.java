package au.com.test.javarobot.core.impl;

import au.com.test.javarobot.IRobot;
import au.com.test.javarobot.Position;
import au.com.test.javarobot.exception.FallOffTableException;
import au.com.test.javarobot.exception.NotPlacedException;

public class Robot implements IRobot {
	
	private final TableTop tableTop;
	private Position position;
		
	public Robot(TableTop tableTop) {
		this.tableTop = tableTop;
	}

	/**
	 * @see {@link IRobot#moveTo(Position)}
	 */
	public void moveTo(Position position) throws FallOffTableException {
		if (tableTop.isFallOff(position)) {
			throw new FallOffTableException("Oops! Out of range now. The table " +
					"range is " + tableTop.getScope());
		}
		this.position = position.clone();
	}
	
	/**
	 * @see {@link IRobot#getCurrentPosition()}
	 */
	public Position getCurrentPosition() throws NotPlacedException {
		if (null == position) {
			throw new NotPlacedException("Oops! Place the robot first." );
		}
		return this.position.clone(); 
	}

}
