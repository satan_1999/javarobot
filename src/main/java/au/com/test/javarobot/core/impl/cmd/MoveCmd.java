package au.com.test.javarobot.core.impl.cmd;

import au.com.test.javarobot.Position;
/**
 * Movement command "Move"
 */
public class MoveCmd extends AbstractMovementCmd {
	
	/**
	 * @see {@link AbstractMovementCmd#handlePosition(Position)}
	 */
	@Override
	public Position handlePosition(Position currentPosition) {
		return currentPosition.getFace().moveForwards(currentPosition.getX(), currentPosition.getY());
	}

}
