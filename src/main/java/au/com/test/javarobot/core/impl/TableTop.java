package au.com.test.javarobot.core.impl;

import au.com.test.javarobot.Position;

/**
 * <p>
 * 	It's a regular rectangle based table top. You can create some other kind
 *  of shapes table top like this class.
 * </p>
 * 
 *
 */
public final class TableTop  {


	private final int dimensionX;
	private final int dimensionY;
	
	public TableTop(int dimensionX, int dimensionY) {
		super();
		this.dimensionX = dimensionX;
		this.dimensionY = dimensionY;
	}
	
	public boolean isFallOff(Position position) {
		int x = position.getX();
		int y = position.getY();
		if ((x >= dimensionX) || (x < 0) || (y <0) || (y >= dimensionY)) {
			return true;
		}
		return false;
	}
	
	public String getScope() {
		return "" + dimensionX + " * "  + dimensionY;
	}
	
	
	public int getDimensionX() {
		return dimensionX;
	}

	public int getDimensionY() {
		return dimensionY;
	}

}
