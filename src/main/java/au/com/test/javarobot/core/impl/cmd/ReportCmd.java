package au.com.test.javarobot.core.impl.cmd;

import au.com.test.javarobot.IRobot;
import au.com.test.javarobot.IStatusCmd;
import au.com.test.javarobot.exception.NotPlacedException;

/**
 * Status command "Report"
 */
public class ReportCmd implements IStatusCmd {
	
	/**
	 * @see {@link IStatusCmd#executeBy(IRobot)}
	 */
	public String executeBy(IRobot robot) throws NotPlacedException {
		return robot.getCurrentPosition().toString();
	}

}
