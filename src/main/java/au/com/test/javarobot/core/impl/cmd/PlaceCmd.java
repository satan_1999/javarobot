package au.com.test.javarobot.core.impl.cmd;

import au.com.test.javarobot.IMovementCmd;
import au.com.test.javarobot.IRobot;
import au.com.test.javarobot.Position;
import au.com.test.javarobot.core.impl.FaceDirection;
import au.com.test.javarobot.exception.FallOffTableException;
/**
 * Movement command "Place"
 */
public class PlaceCmd implements IMovementCmd {
	
	private final Position position;
	
	public PlaceCmd(int x, int y, FaceDirection face) {
		this.position = new Position(x, y, face);
	}
	
	/**
	 * @see {@link IMovementCmd#executeBy(IRobot)}
	 */
	public Position executeBy(IRobot robot) throws FallOffTableException {
		robot.moveTo(position);
		return position.clone();
	}

}
