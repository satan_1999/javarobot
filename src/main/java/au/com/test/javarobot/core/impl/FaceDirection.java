package au.com.test.javarobot.core.impl;

import au.com.test.javarobot.Position;

public enum FaceDirection {
	EAST(1, 0), WEST(-1, 0), NORTH(0, 1), SOUTH(0, -1);

	private int xIncreasement;
	private int yIncreasement;

	private FaceDirection(int xIncreasement, int yIncreasement) {
		this.xIncreasement = xIncreasement;
		this.yIncreasement = yIncreasement;
	}

	public FaceDirection turnRight() {
		switch (this) {
		case EAST:
			return SOUTH;
		case WEST:
			return NORTH;
		case NORTH:
			return EAST;
		case SOUTH:
			return WEST;
		}
		return null;
	}

	public FaceDirection turnLeft() {
		switch (this) {
		case EAST:
			return NORTH;
		case WEST:
			return SOUTH;
		case NORTH:
			return WEST;
		case SOUTH:
			return EAST;
		}
		return null;
	}

	public Position moveForwards(int x, int y) {
		return new Position(x + xIncreasement, y + yIncreasement, this);
	}
}
