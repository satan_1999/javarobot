package au.com.test.javarobot;

import au.com.test.javarobot.exception.FallOffTableException;
import au.com.test.javarobot.exception.NotPlacedException;

/**
 * This interface describe the behavior of movement commands.
 * 
 * <p>
 * 	E.g. the move, left, place and right all belong to this type
 *  which will make the robot to change the position anyway.
 * </p>
 */
public interface IMovementCmd extends IRobotCmd<Position> {
	/**
	 * @see {@link IRobotCmd#executeBy(IRobot)}
	 * 
	 * @throws 
	 * 		{@link NotPlacedException}. If the robot hasn't be placed yet.
	 *      {@link FallOffTableException}. If the robot move out of table top.
	 */
	public Position executeBy(IRobot robot) throws
					NotPlacedException, FallOffTableException;
}
