package au.com.test.javarobot;

import au.com.test.javarobot.exception.NotPlacedException;

/**
 * This interface describe the behavior of status commands.
 * 
 * <p>
 * 	E.g. the command report. it will report the current position of
 *  robot.
 * </p>
 */
public interface IStatusCmd extends IRobotCmd<String> {
	/**
	 * @see {@link IRobotCmd#executeBy(IRobot)}
	 * 
	 * @throws 
	 * 		{@link NotPlacedException}. If the robot hasn't be placed yet.
	 *      
	 */
	public String executeBy(IRobot robot) throws NotPlacedException;
}
