package au.com.test.javarobot.interaction.console.handlers;

import au.com.test.javarobot.IRobotCmd;
import au.com.test.javarobot.Position;
import au.com.test.javarobot.core.impl.cmd.LeftCmd;
import au.com.test.javarobot.core.interaction.IRobotCmdHandler;

/**
 * String to command parser for "Left"
 */
public class LeftCmdHandler implements IRobotCmdHandler<Position, String> {
	private static final LeftCmd cmd = new LeftCmd();
	
	public boolean isConcerned(String t) {
		return "LEFT".equalsIgnoreCase(t.trim());
	}

	public IRobotCmd<Position> parse(String t) {
		return cmd;
	}

}
