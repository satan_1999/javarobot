package au.com.test.javarobot.interaction.console.handlers;

import au.com.test.javarobot.IRobotCmd;
import au.com.test.javarobot.Position;
import au.com.test.javarobot.core.impl.cmd.MoveCmd;
import au.com.test.javarobot.core.interaction.IRobotCmdHandler;

/**
 * String to command parser for "Move"
 */
public class MoveCmdHandler implements IRobotCmdHandler<Position, String> {
	private static final MoveCmd cmd = new MoveCmd();
	
	public boolean isConcerned(String t) {
		return "MOVE".equalsIgnoreCase(t.trim());
	}

	public IRobotCmd<Position> parse(String t){
		return cmd;
	}

}
