package au.com.test.javarobot.interaction.console.handlers;

import au.com.test.javarobot.IRobotCmd;
import au.com.test.javarobot.core.impl.cmd.ReportCmd;
import au.com.test.javarobot.core.interaction.IRobotCmdHandler;

/**
 * String to command parser for "Report"
 */
public class ReportCmdHandler implements IRobotCmdHandler<String, String> {
	private static final ReportCmd cmd = new ReportCmd();
	
	public boolean isConcerned(String t) {
		return "REPORT".equalsIgnoreCase(t.trim());
	}

	public IRobotCmd<String> parse(String t) {
		return cmd;
	}

}
