package au.com.test.javarobot.interaction.console;

import java.util.Scanner;

import au.com.test.javarobot.IRobot;
import au.com.test.javarobot.IRobotCmd;
import au.com.test.javarobot.IStatusCmd;
import au.com.test.javarobot.core.interaction.ICmdHandler;
import au.com.test.javarobot.exception.InvalidCommandException;

/**
 */
public class RobotInteractor {

	private final ICmdHandler<String> cmdHandler;

	private final IRobot robot;

	private boolean notQuit = true;

	private Scanner scanner;

	public RobotInteractor(ICmdHandler<String> cmdHandler, IRobot robot) {
		super();
		this.cmdHandler = cmdHandler;
		this.robot      = robot;
	}

	public void apply(String command) {
		IRobotCmd<?> cmd;
		try {
			cmd = cmdHandler.findCmd(command);
		} catch (InvalidCommandException e1) {
			System.out.println("Wrong command.");
			return;
		} 
		Object rnt;
		try {
			rnt = cmd.executeBy(robot);
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return;
		}
		if (cmd instanceof IStatusCmd) {
			System.out.println(rnt.toString());
		}
	}

	public void init() {
		System.out.println("###########################################################################");
		System.out.println("Thanks for using Li's console robot interactor");
		System.out.println("Please input commands as:");
		System.out.println("	1.  PLACE X,Y,F ");
		System.out.println("	2.  MOVE ");
		System.out.println("	3.  LEFT ");
		System.out.println("	4.  RIGHT ");
		System.out.println("	5.  REPORT ");
		System.out.println("Input quit to exit system");
		System.out.println("###########################################################################");
		scanner = new Scanner(System.in);
	}

	
	// get user's input from console
	public String getInput() {
		String cmd = null;
		cmd = scanner.nextLine();
		return cmd;

	}

	public void start() {
		init();
		String cmd = null;
		while (notQuit && true) {
			System.out.print(">>>");
			cmd = getInput();
			if (cmd.equalsIgnoreCase("quit")) {
				stop();
				System.out.println("system quit, bye!");
				continue;
			}
			apply(cmd);
		}
		scanner.close();

	}

	public void stop() {
		this.notQuit = false;
	}

}
