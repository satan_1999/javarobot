package au.com.test.javarobot.interaction.console.handlers;

import au.com.test.javarobot.IRobotCmd;
import au.com.test.javarobot.Position;
import au.com.test.javarobot.core.impl.FaceDirection;
import au.com.test.javarobot.core.impl.cmd.PlaceCmd;
import au.com.test.javarobot.core.interaction.IRobotCmdHandler;

/**
 */
public class PlaceCmdHandler implements IRobotCmdHandler<Position, String> {

	public boolean isConcerned(String t) {
		return t.trim().toUpperCase().matches(
				"^PLACE\\s+\\d+\\s*,\\s*\\d+\\s*,\\s*(EAST|WEST|SOUTH|NORTH)$");
		
	}

	public IRobotCmd<Position> parse(String t) {
		String uCaseStr    = t.trim().toUpperCase();
		String[] strArray  = uCaseStr.substring("PLACE".length(),uCaseStr.length()).split("\\s*,\\s*");
		int x              = Integer.valueOf(strArray[0].trim());
		int y              = Integer.valueOf(strArray[1].trim());
		FaceDirection face = FaceDirection.valueOf(strArray[2].trim());
		return new PlaceCmd(x, y, face);
	}

}
