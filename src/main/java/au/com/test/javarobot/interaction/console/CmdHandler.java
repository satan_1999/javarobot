package au.com.test.javarobot.interaction.console;

import java.util.HashSet;
import java.util.Set;

import au.com.test.javarobot.IRobotCmd;
import au.com.test.javarobot.core.interaction.ICmdHandler;
import au.com.test.javarobot.core.interaction.IRobotCmdHandler;
import au.com.test.javarobot.exception.InvalidCommandException;

/**
 * Chain of responsibility pattern to find the Command parse;
 * 
 * @param <T>
 *         input type
 *
 */
public class CmdHandler<T> implements ICmdHandler<T> {
	
	
	private Set<IRobotCmdHandler<?, T>> handlers = 	new HashSet<IRobotCmdHandler<?, T>>();
	
	public CmdHandler<T> addHandler(IRobotCmdHandler<?, T> parser) {
		if (null != parser) {
			handlers.add(parser);
		}
		return this;
	}
	
	/**
	 *  We could create a new command parser & a Command and register the
     *  parser in locator, then the system will have new capability to handle
     *  new command.
	 */
	public IRobotCmd<?> findCmd(T t) throws InvalidCommandException {
		if (handlers.size() > 0) {
			for (IRobotCmdHandler<?, T> parser : handlers) {
				if (parser.isConcerned(t)) {
					IRobotCmd<?> cmd = parser.parse(t);
					return cmd;
				}
			}
		}
		throw new InvalidCommandException();
	}

	public int getCmdhandlersCount() {
		return handlers.size();
	}


	
}
