package au.com.test.javarobot.interaction.console.handlers;

import au.com.test.javarobot.IRobotCmd;
import au.com.test.javarobot.Position;
import au.com.test.javarobot.core.impl.cmd.RightCmd;
import au.com.test.javarobot.core.interaction.IRobotCmdHandler;

/**
 * String to command parser for "Right"
 */
public class RightCmdHandler implements IRobotCmdHandler<Position, String> {
	private static final RightCmd cmd = new RightCmd();
	
	public boolean isConcerned(String t) {
		return "RIGHT".equalsIgnoreCase(t.trim());
	}

	public IRobotCmd<Position> parse(String t) {
		return cmd;
	}

}
