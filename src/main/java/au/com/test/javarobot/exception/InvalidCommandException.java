package au.com.test.javarobot.exception;

/**
 * When the command, this exception
 * will be thrown out. 
 */
public class InvalidCommandException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 232338711822959L;

	public InvalidCommandException() {
		super("The command can't be recognized.");
	}
	
	

}
