package au.com.test.javarobot.exception;

/**
 * When the command parsing is failed, this exception will be
 * thrown out.
 */
public class CmdHandlerException extends RuntimeException {

	public CmdHandlerException(Exception e) {
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = -2282513094662L;

	public CmdHandlerException(String arg0, Throwable arg1) {
		super(arg0, arg1);
	}

	public CmdHandlerException(String arg0) {
		super(arg0);
	}

}
