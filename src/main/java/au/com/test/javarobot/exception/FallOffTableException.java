package au.com.test.javarobot.exception;

/**
 * When the robot move out of the range of the table top, this exception 
 * will be thrown out.
 */
public class FallOffTableException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8567073597354354798L;

	public FallOffTableException() {
		super();
	}

	public FallOffTableException(String arg0) {
		super(arg0);
	}
}
