package au.com.test.javarobot.exception;


/**
 * When the robot hasn't been put while invoke other command, this exception
 * will be thrown out. 
 *
 */
public class NotPlacedException extends RuntimeException {
	/**
	 * 
	 */
	private static final long serialVersionUID = 23326100L;

	public NotPlacedException(String arg0) {
		super(arg0);
	}
	
	

}
