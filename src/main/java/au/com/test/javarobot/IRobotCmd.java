package au.com.test.javarobot;


/**
 * The command the robot may execute.
 * @param <T>
 * 			The result type, if the command is executed by robot.
 *          For the movement command, it will be {@link Position}
 *          For the status command. E.g. report, it will be String.
 */
public interface IRobotCmd<T> {
	/**
	 * Command the robot will executed.
	 * 
	 * @param robot
	 * @return
	 * @throws Exception
	 * 			Generic exception, specific exceptions will be thrown out
	 *          in realization.
	 */
	public T executeBy(IRobot robot) throws Exception;		
}
